# Game Dev Field Guide Bot

This is a community bot for the GDFG community.

### How it works

[TBC]

### Contribute

**New Commands**

 - See a list of Commands that have been requested (ordered by popularity) [here](https://gitlab.com/yanniboi/gdfg-bot/-/issues?label_name%5B%5D=command&scope=all&sort=popularity&state=opened)
 - You can vote on Commands that you like/want by clicking the thumbs up/down on the issue page.


**Other**

 - Create code and submit a merge request.
 - Report a bug

 [Details to follow...]

### Contact

 - Join the [discord](https://discord.gg/2C8eTsU) community and say hi.
