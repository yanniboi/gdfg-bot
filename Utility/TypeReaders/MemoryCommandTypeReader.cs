using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Microsoft.VisualBasic;

namespace gdfg_bot.Utility.TypeReaders
{
    public class MemoryCommandTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            MemoryCommand result = new MemoryCommand();
            
            string[] parts = input.Split("=");

            if (parts.Length < 2)
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "The format of the command is wrong"));
            }
            if (parts.Length > 2)
            {
                List<string> combine = parts.ToList();
                combine.RemoveAt(0);
                string combinedText = string.Join("=", combine);
                parts = new[] {parts[0], combinedText};
            }

            result.Name = Strings.Trim(parts[0]);
            result.Text = Strings.Trim(parts[1]);
            return Task.FromResult(TypeReaderResult.FromSuccess(result));
        }
    }
}