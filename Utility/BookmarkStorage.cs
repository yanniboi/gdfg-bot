using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace gdfg_bot.Utility
{
    public class BookmarkStorage
    {
        private const string BookmarksFileName = "bookmarks.json";

        [JsonProperty("bookmarks")]
        public List<Bookmark> Bookmarks { get; set; } = new List<Bookmark>();

        public static BookmarkStorage Load()
        {
            if (File.Exists(BookmarksFileName))
            {
                var json = File.ReadAllText(BookmarksFileName);
                return JsonConvert.DeserializeObject<BookmarkStorage>(json);
            }
            var db = new BookmarkStorage();
            db.Save();
            return db;
        }

        public void Save()
        {
            var json = JsonConvert.SerializeObject(this);
            File.WriteAllText(BookmarksFileName, json);
        }

        public BookmarkStorage Add(Bookmark bookmark)
        {
            if (!Exists(bookmark))
            {
                Bookmarks.Add(bookmark);
            }

            return this;
        }

        public BookmarkStorage Remove(Bookmark bookmark)
        {
            Bookmark removeItem = Bookmarks
                .FirstOrDefault(x => x.OwnerId == bookmark.OwnerId
                              && x.MessageId == bookmark.MessageId);
            if (removeItem != null)
            {
                Bookmarks.Remove(removeItem);
            }
            return this;
        }

        private bool Exists(Bookmark bookmark)
        {
            return null != Bookmarks
                .FirstOrDefault(x => x.OwnerId == bookmark.OwnerId
                                     && x.MessageId == bookmark.MessageId);
        }

    }

    public sealed class Bookmark
    {
        private int messageLength = 50;

        public Bookmark(ulong user, ulong message)
        {
            MessageId = message;
            OwnerId = user;
        }

        public ulong MessageId { get; set; }
        public ulong OwnerId { get; set; }
        public string MessageText
        {
            get => _trimmedMessage;
            set =>
                _trimmedMessage = value.Length > messageLength
                    ? value.Substring(0, messageLength) + "..."
                    : value;
        }

        public string Channel { get; set; }
        public string Url { get; set; }
        public string Poster { get; set; }
        public string Owner { get; set; }
        public DateTime Timestamp { get; set; }

        private string _trimmedMessage;

        [JsonIgnore]
        public string CommandMessageLabel => $"Post by {Poster} in #{Channel} on {Timestamp:g}";

        [JsonIgnore]
        public string CommandMessageValue => $"{MessageText} - {Url}";
    }
}
