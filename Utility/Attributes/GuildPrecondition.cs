using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace gdfg_bot.Utility.Attributes
{
    public class RequireGuildAttribute : PreconditionAttribute
    {
        
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Guild == null)
                return Task.FromResult(PreconditionResult.FromError("This command may only be run in a guild server."));
            
            return Task.FromResult(PreconditionResult.FromSuccess());
        }
    }
}