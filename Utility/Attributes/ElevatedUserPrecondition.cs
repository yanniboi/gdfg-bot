using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace gdfg_bot.Utility.Attributes
{
    public class RequireElevatedUserAttribute : PreconditionAttribute
    {
        
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            if (context.Guild == null)
                return Task.FromResult(PreconditionResult.FromError("This command may only be run in a guild server."));

            IConfiguration config = services.GetService<IConfiguration>();

            IConfigurationSection rolesConfig = config?.GetSection("Roles");
            List<string> roles = new List<string>();
            foreach (var role in rolesConfig.AsEnumerable())
            {
                if (!string.IsNullOrEmpty(role.Value))
                {
                    roles.Add(role.Value);
                }
            }
            
            if (roles.Count == 0)
                return Task.FromResult(PreconditionResult.FromError("Please configure your elevated roles."));

            if (!(context.User as SocketGuildUser).Roles.Any(id => roles.Contains(id.Id.ToString())))
                return Task.FromResult(PreconditionResult.FromError("You do not have an elevated role."));

            return Task.FromResult(PreconditionResult.FromSuccess());
        }
    }
}