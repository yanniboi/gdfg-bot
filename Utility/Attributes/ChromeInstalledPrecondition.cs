using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace gdfg_bot.Utility.Attributes
{
    public class RequireChromeAttribute : PreconditionAttribute
    {
        
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            return Task.FromResult(PreconditionResult.FromError("Chrome browser is not installed on server."));
        }
    }
}