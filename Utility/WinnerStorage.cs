using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace gdfg_bot.Utility
{
    public class WinnerStorage
    {
        public WinnerDb Database { get; }

        private List<WinnerScore> _scores; 
        private List<WinnerStreak> _streaks; 
        
        public WinnerStorage()
        {
            Database = WinnerDb.Load();
        }

        public void AddWinner(Winner winner)
        {
            Database.Winners.Add(winner);
            Database.Save();
        }
        public void RemoveWinner(Winner winner)
        {
            Database.Winners.Remove(winner);
            Database.Save();
        }

        public List<Winner> GetWinners()
        {
            return Database.Winners;
        }
        
        public List<ContestWinners> GetWinnersByContest()
        {
            List<Winner> winners = Database.Winners;
            var winnersByContest = winners.GroupBy(w => w.Contest).ToList();

            List<ContestWinners> results = new List<ContestWinners>();
            foreach (var groupOfWins in winnersByContest)
            {
                List<Winner> contestWinners = new List<Winner>();

                
                foreach (var winner in groupOfWins)
                {
                   contestWinners.Add(winner);
                }
                ContestWinners contestWithWinners = new ContestWinners(groupOfWins.Key, contestWinners);
                results.Add(contestWithWinners);
            }

            return results;
        }
        
        public List<WinnerScore> GetWinnersScores()
        {
            SetScores();
            return _scores;
        }
        
        public List<WinnerStreak> GetStreaks()
        {
            SetStreaks();
            return _streaks;
        }

        public int CountChallenges()
        {
            List<long> timestamps = Database.Winners.Select(t => t.Timestamp).ToList();
            var grouped = timestamps.GroupBy(w => w);
            return grouped.Count(); 
        }

        public int GetOffset()
        {
            return Database.Offset;
        }

        public void SkipChallenge()
        {
            Database.Offset++;
            Database.Save();
        }

        public void UnskipChallenge()
        {
            Database.Offset--;
            Database.Save();
        }

        private void SetStreaks()
        {
            List<Winner> winners = Database.Winners.Select(t => t).ToList();
            var winnersByWinTimestamp = winners.GroupBy(w => w.Timestamp).ToList();
            
            winnersByWinTimestamp.Sort(delegate(IGrouping<long, Winner> x, IGrouping<long, Winner> y)
            {
                var compareTo = x.Key.CompareTo(y.Key);
                return compareTo;
            });

            List<WinnerStreak> streaks = new List<WinnerStreak>();
            Dictionary<ulong, WinnerStreak> usersCurrentStreakMap = new Dictionary<ulong, WinnerStreak>();
            List<ulong> previousWinners = new List<ulong>();
            List<ulong> streakEnded = new List<ulong>();
            foreach (var groupOfWins in winnersByWinTimestamp)
            {
                List<ulong> currentWinners = new List<ulong>();
                List<Winner> repeatWinners = new List<Winner>();
                foreach (var winner in groupOfWins)
                {
                    currentWinners.Add(winner.UserId);
                    if (previousWinners.Contains(winner.UserId))
                    {
                        repeatWinners.Add(winner);
                    }
                }

                foreach (var previous in previousWinners)
                {
                    if (!currentWinners.Contains(previous) && usersCurrentStreakMap.ContainsKey(previous))
                    {
                        WinnerStreak previousStreak = usersCurrentStreakMap[previous];
                        previousStreak.Current = false;
                        streakEnded.Add(previous);
                    }
                }

                previousWinners = currentWinners;

                foreach (Winner repeatWinner in repeatWinners)
                {
                    ulong repeatWinnerId = repeatWinner.UserId;
                    if (!usersCurrentStreakMap.ContainsKey(repeatWinnerId))
                    {
                        WinnerStreak streak = new WinnerStreak(usersCurrentStreakMap.Count(), repeatWinnerId);
                        streak.Name = repeatWinner.Name;
                        usersCurrentStreakMap[repeatWinnerId] = streak;
                        streaks.Add(streak);
                    }

                    if (streakEnded.Contains(repeatWinnerId))
                    {
                        WinnerStreak streak = new WinnerStreak(usersCurrentStreakMap.Count(), repeatWinnerId);
                        usersCurrentStreakMap[repeatWinnerId] = streak;
                        streaks.Add(streak);
                        streakEnded.Remove(repeatWinnerId);
                    }

                    usersCurrentStreakMap[repeatWinnerId].Score++;
                }
            }

            streaks.Sort(delegate(WinnerStreak x, WinnerStreak y)
            {
                var compareTo = y.Score.CompareTo(x.Score);
                return compareTo;
            });
        
            _streaks = streaks;
        }
        
        private void SetScores()
        {
            List<Winner> winners = Database.Winners.Select(t => t).ToList();
            var g = winners.GroupBy(w => w.UserId);
            
            
            List<WinnerScore> orderedWinners = new List<WinnerScore>();
            foreach (var grp in g)
            {
                var winnerScore = new WinnerScore(grp.Key, grp.Count());
                winnerScore.Name = grp.First().Name;
                orderedWinners.Add(winnerScore);
            }
            
            orderedWinners.Sort(delegate(WinnerScore x, WinnerScore y)
            {
                var compareTo = y.Score.CompareTo(x.Score);
                return compareTo;
            });

            _scores = orderedWinners;
        }
    }

    public class WinnerDb
    {
        private WinnerDb() { }

        [JsonProperty("winners")]
        public List<Winner> Winners { get; set; } = new List<Winner>();
        public int Offset { get; set; } = 4;

        public static WinnerDb Load()
        {
            if (File.Exists("winners.json"))
            {
                var json = File.ReadAllText("winners.json");
                return JsonConvert.DeserializeObject<WinnerDb>(json);
            }
            var db = new WinnerDb();
            db.Save();
            return db;
        }

        public void Save()
        {
            var json = JsonConvert.SerializeObject(this);
            File.WriteAllText("winners.json", json);
        }
    }

    public sealed class Winner
    {
        public string Name { get; set; }
        public ulong UserId { get; set; }
        public string Creator { get; set; }
        public ulong CreatorId { get; set; }
        public string Contest { get; set; }
        public long Timestamp { get; set; }
    }
    
    public sealed class ContestWinners
    {
        public string Contest { get; set; }
        public List<Winner> Winners { get; set; }
        
        public ContestWinners(string contest, List<Winner> winners)
        {
            Contest = contest;
            Winners = winners;
        }
    }
    
    public sealed class WinnerScore
    {
        public WinnerScore(ulong id, int score)
        {
            Id = id;
            Score = score;
        }
        public ulong Id { get; set; }
        public int Score { get; set; }
        public string Name { get; set; }
    }
    
    public sealed class WinnerStreak
    {
        public WinnerStreak(int id, ulong userId)
        {
            Id = id;
            UserId = userId;
            Score = 1;
            Current = true;
        }
        public int Id { get; set; }
        public ulong UserId { get; set; }
        public int Score { get; set; }
        public bool Current { get; set; }
        public string Name { get; set; }
    }

}
