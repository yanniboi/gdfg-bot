using System.Collections.Generic;
using HtmlAgilityPack;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.IO;
using System.Linq;
using PuppeteerSharp;


namespace gdfg_bot.Utility
{
    public class WebScraper
    {
        public static async Task<string> CallUrl(string fullUrl)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13;
            client.DefaultRequestHeaders.Accept.Clear();
            var response = client.GetStringAsync(fullUrl);
            return await response;
        }
        
        public static async Task<string[]> CallUrlJs(string fullUrl, string className)
        {
            var options = new LaunchOptions()
            {
                Headless = true,
                ExecutablePath = "/opt/brave.com/brave/brave-browser"
            };
            var browser = await Puppeteer.LaunchAsync(options, null);
            var page = await browser.NewPageAsync();
            await page.GoToAsync(fullUrl);
            
            var links = @"Array.from(document.querySelectorAll('" + className + "')).map(a => a.href);";
            var urls = page.EvaluateExpressionAsync<string[]>(links);
          
            return await urls;
        }
        
        public List<string> ParseHtml(string html, string container, string className)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            var links = htmlDoc.DocumentNode.Descendants(container)
                .Where(node => node.GetAttributeValue("class", "").Equals(className)).ToList();

            List<string> foundLinks = new List<string>();

            foreach (var link in links)
            {
                var child = link.FirstChild;
                while (child.Name != "a")
                {
                    if (!child.HasChildNodes)
                    {
                        break;
                    }

                    child = child.FirstChild;
                }

                if (child?.Name == "a" && child?.Attributes?.Count > 0)
                    foundLinks.Add(child.Attributes[0].Value) ;
            }

            return foundLinks;
        }
    }
}