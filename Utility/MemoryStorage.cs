using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace gdfg_bot.Utility
{
    public class MemoryStorage
    {
        private const string MemoriesFileName = "memories.json";

        [JsonProperty("memories")]
        public List<MemoryCommand> MemoryCommands { get; set; } = new List<MemoryCommand>();

        public static MemoryStorage Load()
        {
            if (File.Exists(MemoriesFileName))
            {
                var json = File.ReadAllText(MemoriesFileName);
                return JsonConvert.DeserializeObject<MemoryStorage>(json);
            }
            var db = new MemoryStorage();
            db.Save();
            return db;
        }

        public void Save()
        {
            var json = JsonConvert.SerializeObject(this);
            File.WriteAllText(MemoriesFileName, json);
        }
    }

    public sealed class MemoryCommand
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public ulong OwnerId { get; set; }
        public string Owner { get; set; }
    }
}
