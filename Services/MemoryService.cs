using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using gdfg_bot.Utility;

namespace gdfg_bot.Services
{
        public class MemoryService
        {

            public MemoryStorage Database { get; }

            private readonly CommandService _commands;

            private ModuleInfo _rememberModule;

            public MemoryService(CommandService commands)
            {
                _commands = commands;
                Database = MemoryStorage.Load();
                BuildCommands().GetAwaiter().GetResult();
            }

            public async Task BuildCommands()
            {
                if (_rememberModule != null)
                    await _commands.RemoveModuleAsync(_rememberModule);

                _rememberModule = await _commands.CreateModuleAsync("", module =>
                {
                    module.Name = "Remember";

                    foreach (var memories in Database.MemoryCommands)
                    {
                        module.AddCommand(memories.Name, async (context, args, map, cheese) =>
                        {
                            var builder = new EmbedBuilder()
                                .WithTitle(memories.Name)
                                .WithDescription(memories.Text);
                            var user = await context.Channel.GetUserAsync(memories.OwnerId);
                            if (user != null)
                                builder.Author = new EmbedAuthorBuilder()
                                    .WithIconUrl(user.GetAvatarUrl())
                                    .WithName(user.Username);

                            await context.Channel.SendMessageAsync("", embed: builder.Build());
                        }, builder => { builder.AddAliases(); });
                    }
                });
            }

            public bool CheckMemoryExists(string memoryName)
            {
                List<CommandInfo> commands = _commands.Commands.ToList();
                return commands.Exists(x => x.Aliases.Contains(memoryName));
            }

            public async Task AddMemory(MemoryCommand memory)
            {
                Database.MemoryCommands.Add(memory);
                Database.Save();
                await BuildCommands();
            }
            
            public async Task RemoveMemory(MemoryCommand memory)
            {
                Database.MemoryCommands.Remove(memory);
                Database.Save();
                await BuildCommands();
            }
        }
}