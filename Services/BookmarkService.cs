using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.EmojiTools;
using Discord.WebSocket;
using gdfg_bot.Utility;
using Microsoft.Extensions.DependencyInjection;

namespace gdfg_bot.Services
{
        public class BookmarkService
        {

            public static readonly IEmote BookmarkEmoji = EmojiExtensions.FromText("bookmark");
            public static readonly IEmote GdfgBookmarkEmoji = Emote.Parse("<:GDFG_Bookmark:882300891347157052>");
            public static readonly IEmote GdfgBookmarkAnimatedEmoji = Emote.Parse("<a:GDFG_Bookmark_Ani:882274970267443220>");

            private readonly List<IEmote> _validEmojis = new List<IEmote>();

            private BookmarkStorage Database { get; }

            public BookmarkService(IServiceProvider services)
            {
                DiscordSocketClient client = services.GetRequiredService<DiscordSocketClient>();
                client.ReactionAdded += HandleReactionAddedAsync;
                client.ReactionRemoved += HandleReactionRemovedAsync;

                _validEmojis.Add(BookmarkEmoji);
                _validEmojis.Add(GdfgBookmarkEmoji);
                _validEmojis.Add(GdfgBookmarkAnimatedEmoji);

                Database = BookmarkStorage.Load();
            }

            private async Task HandleReactionAddedAsync(Cacheable<IUserMessage, ulong> cachedMessage,
                ISocketMessageChannel originChannel, SocketReaction reaction)
            {
                if (!IsValidReaction(reaction))
                {
                    return;
                }

                var message = await cachedMessage.GetOrDownloadAsync();
                if (message == null)
                {
                    return;
                }

                Bookmark bookmark = BuildBookmarkFromReaction(reaction, message);
                Database.Add(bookmark).Save();
            }

            private async Task HandleReactionRemovedAsync(Cacheable<IUserMessage, ulong> cachedMessage,
                ISocketMessageChannel originChannel, SocketReaction reaction)
            {
                if (!IsValidReaction(reaction))
                {
                    return;
                }

                var message = await cachedMessage.GetOrDownloadAsync();
                if (message == null)
                {
                    return;
                }

                Bookmark bookmark = new Bookmark(reaction.UserId, message.Id);
                Database.Remove(bookmark).Save();
            }

            private bool IsValidReaction(SocketReaction reaction)
            {
                if (reaction.User.IsSpecified && _validEmojis.Contains(reaction.Emote))
                {
                    return true;
                }
                return false;
            }

            private Bookmark BuildBookmarkFromReaction(SocketReaction reaction, IMessage message)
            {
                Bookmark bookmark = new Bookmark(reaction.UserId, message.Id)
                {
                    MessageText = message.Content,
                    Channel = message.Channel.ToString(),
                    Poster = message.Author.ToString(),
                    Owner = reaction.User.ToString(),
                    Url = message.GetJumpUrl(),
                    Timestamp = Convert.ToDateTime(message.Timestamp.ToString())
                };

                return bookmark;
            }

            public List<Bookmark> GetBookmarksByUser(ulong userId)
            {
                var bookmarks = Database.Bookmarks
                    .Where(x => x.OwnerId == userId)
                    .OrderBy(x => x.Timestamp)
                    .ToList();
                return bookmarks;
            }
        }
}
