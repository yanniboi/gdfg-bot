### Deployment

```bash
cd gdfg-bot/
git fetch
git checkout [LATEST_TAG]
dotnet nuget add source  -n "discord.net ci feed" https://www.myget.org/F/discord-net/api/v3/index.json
dotnet publish -o ~/bot
dotnet ~/bot/gdfg-bot.dll > ~/bot/bot.log 2>&1 &
```
