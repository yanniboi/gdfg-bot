using System.Threading.Tasks;
using Discord.Commands;

namespace gdfg_bot.Modules
{
    public class PingCommands : ModuleBase
    {
        [Command("ping")]
        [Summary("Are you there bot?")]
        public async Task PingCommand()
        {
            await ReplyAsync("pong");
        }
    }
}
