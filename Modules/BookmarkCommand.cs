using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using gdfg_bot.Services;
using gdfg_bot.Utility;

namespace gdfg_bot.Modules
{
    public class BookmarkCommands : ModuleBase
    {

        private readonly BookmarkService _service;

        public BookmarkCommands(BookmarkService service)
        {
            _service = service;
        }

        [Command("bookmarks")]
        [Priority(1000)]
        [Summary("Ask the bot to list your bookmarks.")]
        public async Task BookmarksCommand()
        {
            _ = Context.Message.DeleteAsync().ConfigureAwait(false);
            List<Bookmark> bookmarks = _service.GetBookmarksByUser(Context.User.Id);

            if (bookmarks.Count == 0)
            {
                await Context.User.SendMessageAsync($"Looks like the you dont have any bookmarks yet... React to posts with {BookmarkService.GdfgBookmarkEmoji} or {BookmarkService.GdfgBookmarkAnimatedEmoji} to bookmark them.");
                return;
            }

            var embed = new EmbedBuilder();
            embed.WithTitle("Your GDFG Bookmarks");
            foreach (var bookmark in bookmarks)
            {
                embed.AddField(bookmark.CommandMessageLabel, bookmark.CommandMessageValue);
            }

            await Context.User.SendMessageAsync("", embed: embed.Build());
        }
    }
}
