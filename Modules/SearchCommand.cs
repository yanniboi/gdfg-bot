using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.Interactive;
using Discord.Commands;
using gdfg_bot.Utility;
using gdfg_bot.Utility.Attributes;
using HtmlAgilityPack;

namespace gdfg_bot.Modules
{
    public class SearchCommands : ModuleBase<SocketCommandContext>
    {
        private readonly InteractiveService _interactive;

        public SearchCommands(InteractiveService interactive)
        {
            _interactive = interactive;
        }
        
        [Command("search unity")]
        [Alias("unity")]
        [RequireChrome]
        public async Task UnityCommand(string query)
        {
            string url = "https://docs.unity3d.com/ScriptReference/30_search.html?q=";
            var links = WebScraper.CallUrlJs(url + query, "div.result > a").Result.Take(1).ToList();

            if (links.Count > 0)
            {
                await ReplyAsync(links[0]);
            }
            else
            {
                await ReplyAsync("No result found.");
            }
        }
        
        [Command("search itch jams")]
        [Alias("random jam", "itch jam")]
        [Summary("Find a random jam from this list: https://itch.io/jams/starting-this-month.")]
        public async Task ItchJamCommand()
        {
            string baseUrl = "https://itch.io";
            string jamUrl = baseUrl + "/jams/starting-this-month";
            WebScraper scraper = new WebScraper();
            var page = WebScraper.CallUrl(jamUrl).Result;
            var links = scraper.ParseHtml(page, "div", "primary_info").ToList();

            if (links.Count > 0)
            {
                var random = new Random();
                int index = random.Next(links.Count);
                string jamLink = baseUrl + links[index];
                await ReplyAsync(jamLink);
            }
            else
            {
                await ReplyAsync("No result found.");
            }
        }
        
        [Command("search itch games", RunMode = RunMode.Async)]
        [Alias("jam game", "itch game", "random game")]
        [Summary("Find a random game jam submission from itch.io to play and review.")]
        public async Task ItchGameCommand()
        {
            var jams = Jam.GetJams();
            
            if (jams.Count > 0)
            {
                EmbedBuilder embedBuilder = new EmbedBuilder();
 
                int i = 1;
                jams.ForEach((jam) =>
                {
                    string jamStats = String.Join(", ", jam.GetStats());
                    string jamTitle = jam.GetTitle();
                    embedBuilder.AddField($"{i} - {jamTitle}", jamStats);
                    i++;
                });
               

                await ReplyAsync("**Which jam would you like to search?** _'cancel' to cancel_", embed: embedBuilder.Build());
                var nameResponse = await _interactive.NextMessageAsync(Context, true, true, TimeSpan.FromSeconds(180));
                if (nameResponse.Content == "cancel") return;

                int response;
                try
                {
                    response = int.Parse(nameResponse.Content);
                    if (response > 5) return;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to parse '{0}'.", nameResponse.Content);
                    await ReplyAsync("That was not an option.");
                    return;
                }

                Jam selectedJam = jams[response - 1];
                var sb = new StringBuilder($"You selected {selectedJam.GetTitle()}");

                string submissionUrl = selectedJam.GetLink() + "/entries";
                List<Submission> submissions = Jam.GetEntries(submissionUrl);
                var random = new Random();
                int index = random.Next(submissions.Count);
                Submission selectedSubmission = submissions[index];

                sb.AppendLine();
                sb.Append($"Your chosen game is {selectedSubmission.Title} - {selectedSubmission.Link}");
                await ReplyAsync(sb.ToString());

            }
            else
            {
                await ReplyAsync("No result found.");
            }
        }
        
        public sealed class Jam
        {
            private const string BaseUrl = "https://itch.io";
            private const string JamUrl = BaseUrl + "/jams/past/sort-date";

            private readonly HtmlNode _html;
            private string _title = "";
            private string _link = "";
            private readonly List<string> _stats = new List<string>();

            private Jam(HtmlNode html)
            {
                _html = html;
            }

            public static List<Jam> GetJams()
            {
                var page = WebScraper.CallUrl(JamUrl).Result;
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(page);
                List<HtmlNode> jamContainer = htmlDoc.DocumentNode.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Equals("jam_grid_widget")).ToList();

                List<HtmlNode> jamNodes = jamContainer[0].ChildNodes.ToList();

                List<Jam> jams = new List<Jam>();

                for (int i = 0; i < MathF.Min(5, jamNodes.Count); i++)
                {
                    jams.Add(new Jam(jamNodes[i]));
                }

                return jams;
            }
            public static List<Submission> GetEntries(string url)
            {
                var page = WebScraper.CallUrl(url).Result;
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(page);
                List<HtmlNode> entryNodes = htmlDoc.DocumentNode.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Contains("game_cell")).ToList();

                List<Submission> submissions = new List<Submission>();
                
                foreach (var entryNode in entryNodes)
                {
                    foreach (var child in entryNode.ChildNodes)
                    {
                        if (child.HasClass("game_cell_data"))
                        {
                            HtmlNode labelChild = child;
                            while (labelChild.Name != "a")
                            {
                                if (!labelChild.HasChildNodes)
                                {
                                    break;
                                }

                                labelChild = labelChild.FirstChild;
                            }

                            if (labelChild.Name == "a" && labelChild.Attributes?.Count > 0)
                            {
                                Submission submission = new Submission();

                                foreach (var attribute in labelChild.Attributes)
                                {
                                    if (attribute.Name == "href")
                                    {
                                        submission.Link = attribute.Value;
                                        break;
                                    }
                                }
                                submission.Title = labelChild.InnerText;
                                submissions.Add(submission);
                            }
                            break;
                        }
                    }
                    
                }
                
                return submissions;
            }
            
            public string GetLink()
            {
                if (_link == "")
                {
                    var links = _html.ChildNodes.Descendants("div")
                        .Where(node => node.GetAttributeValue("class", "").Equals("primary_info")).ToList();
                
                    foreach (var link in links)
                    {
                        var child = link.FirstChild;
                        while (child.Name != "a")
                        {
                            if (!child.HasChildNodes)
                            {
                                break;
                            }

                            child = child.FirstChild;
                        }

                        if (child.Name == "a" && child.Attributes?.Count > 0)
                        {
                            _link = BaseUrl + child.Attributes[0].Value;
                            break;
                        }
                    }
                }

                return _link;
            }


            public string GetTitle()
            {
                if (_title == "")
                {
                    var links = _html.ChildNodes.Descendants("div")
                        .Where(node => node.GetAttributeValue("class", "").Equals("primary_info")).ToList();
                
                    foreach (var link in links)
                    {
                        var child = link.FirstChild;
                        while (child.Name != "a")
                        {
                            if (!child.HasChildNodes)
                            {
                                break;
                            }

                            child = child.FirstChild;
                        }

                        if (child.Name == "a" && child.Attributes?.Count > 0)
                        {
                            _title = child.InnerText;
                            break;
                        }
                    }
                }

                return _title;
            }

            public List<string> GetStats()
            {
                if (_stats.Count == 0)
                {
                    var statsContainer = _html.ChildNodes.Descendants("div")
                        .Where(node => node.GetAttributeValue("class", "").Contains("jam_stats")).ToList();

                    foreach (var stat in statsContainer[0].ChildNodes)
                    {
                        _stats.Add(stat.InnerText);
                    }
                }
                
                return _stats;
            }
        }

        public sealed class Submission
        {
            public string Title { get; set; }
            public string Link { get; set; }
        }
    }
}