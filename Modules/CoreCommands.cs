using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace gdfg_bot.Modules
{
    public class CoreCommands : ModuleBase<SocketCommandContext>
    {
        
        private readonly CommandService _commandService;
        public CoreCommands(CommandService commands)
        {
            _commandService = commands;
        }
        
        [Command("help")]
        [Summary("Shows a list of commands that the bot understands.")]
        public async Task Help()
        {
            List<CommandInfo> commands = _commandService.Commands.ToList();
            EmbedBuilder embedBuilder = new EmbedBuilder();

            foreach (CommandInfo command in commands)
            {
                if (command.Summary == null)
                {
                    continue;
                }

                // @todo check config for command prefix.
                string commandPrefix = "!";
                string embedFieldName = $"{commandPrefix}{command.Name}";
                StringBuilder embedFieldText = new StringBuilder();
                if (command.Aliases.Count > 1)
                {
                    // Remove first alias as that is the main command name.
                    List<string> aliases = command.Aliases.ToList();
                    aliases.RemoveAt(0);
                    embedFieldText.Append($"*Aliases: {commandPrefix}{string.Join($", {commandPrefix}", aliases)}*");
                    embedFieldText.AppendLine();
                }
                embedFieldText.Append(command.Summary ?? "No description available");
                embedBuilder.AddField(embedFieldName, embedFieldText);
            }

            await ReplyAsync("Here's a list of commands and how to use them: ", false, embedBuilder.Build());
        }
    }
}