using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using gdfg_bot.Services;

namespace gdfg_bot.Modules
{
    public class TwitchCommand : ModuleBase<SocketCommandContext>
    {

        [Command("live")]
        [Summary("Use for Twitch go-live notifications.")]
        public async Task GoLiveCommand([Remainder] string _)
        {
            CommandHandler.LivePosts.Add(Context.Message);
            Console.WriteLine($"Twitch message logged for post [{Context.Message.Id.ToString()}] will be deleted later.");
        }
    }
}
