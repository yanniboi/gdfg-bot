using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.EmojiTools;
using Discord.Commands;
using gdfg_bot.Services;
using gdfg_bot.Utility;
using gdfg_bot.Utility.Attributes;

namespace gdfg_bot.Modules
{
    public class RememberCommands : ModuleBase
    {

        private readonly MemoryService _service;

        public RememberCommands(MemoryService service)
        {
            _service = service;
        }

        [Command("remember")]
        [Priority(1000)]
        [Summary("Tell the bot to remember something. For you to share later." +
                 "\r\n*Usage*: command name = information to remember")]
        public async Task RememberCommand(
            [Remainder] MemoryCommand memory)
        {
            memory.OwnerId = Context.User.Id;
            memory.Owner = Context.User.Username;

            if (_service.CheckMemoryExists(memory.Name))
            {
                await ReplyAsync($"Looks like the {memory.Name} command already exists, try another name.");
                return;
            }
            else
            {
                await _service.AddMemory(memory);
            }

            var embed = new EmbedBuilder()
                .WithTitle(memory.Name)
                .WithDescription(memory.Text)
                .WithFooter($"Use !{memory.Name} in chat and I will remind you of this.")
                .WithAuthor(x =>
                {
                    x.IconUrl = Context.User.GetAvatarUrl();
                    x.Name = Context.User.Username;
                })
                .Build();
            await ReplyAsync("I will remember that.", embed: embed);
        }
        
        [Command("forget")]
        [Priority(1000)]
        [RequireElevatedUser]
        public async Task ForgetCommand([Remainder] string name)
        {
            var memory = _service.Database.MemoryCommands.FirstOrDefault(x => x.Name == name);
            if (memory == null)
            {
                await ReplyAsync("No reminders found.");
                return;
            }
            
            await _service.RemoveMemory(memory);
            await ReplyAsync($"{EmojiExtensions.FromText(":put_litter_in_its_place:")}");
        }
        
        [Command("reminders")]
        [Alias("memories")]
        [Priority(1000)]
        [Summary("What do you know bot? List of commands for helpful reminders.")]
        public async Task ListMemoriesCommand()
        {
            await ReplyAsync($"I have learnt the following commands: **{string.Join("**, **", _service.Database.MemoryCommands.Select(t => t.Name))}**");
        }
    }
}