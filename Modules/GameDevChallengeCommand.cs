using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.EmojiTools;
using Discord.Commands;
using Discord.WebSocket;
using gdfg_bot.Utility;
using gdfg_bot.Utility.Attributes;

namespace gdfg_bot.Modules
{
    public class GameDevChallengeCommands : ModuleBase
    {
        private readonly WinnerStorage _storage;

        public GameDevChallengeCommands()
        {
            _storage = new WinnerStorage();
        }

        [Command("challenge winner")]
        [Alias("winner")]
        [Priority(1000)]
        [RequireElevatedUser]
        public async Task AddTagAsync(
            params string[] winUsers)
        {
            if (winUsers.Length < 1)
            {
                await ReplyAsync("Please specify a winner.");
                return;
            }

            var now = DateTime.UtcNow;
            long unixTime = ((DateTimeOffset) now).ToUnixTimeSeconds();

            int count = _storage.CountChallenges() + _storage.GetOffset();
            string contest = $"Episode #{count} Challenge";

            StringBuilder sb = new StringBuilder();

            // Backup code for if winUsers is array of strings.
            var users = (await Context.Guild.GetUsersAsync());

            foreach (var userId in winUsers)
            {
                // Backup code for if winUsers is array of strings.
                var id = userId.Trim('@', '!', '<', '>');
                var filteredUsers = users.Where(f =>
                f.Id.ToString() == id || f.Nickname == userId || f.Username == userId);
                
                var user = filteredUsers.FirstOrDefault();    
                
                if (user != null)
                {
                    var winner = new Winner
                    {
                        Contest = contest,
                        Timestamp = unixTime,
                        UserId = user.Id,
                        Name = user.Username,
                        CreatorId = Context.User.Id,
                        Creator = Context.User.Username,
                    };
                    _storage.AddWinner(winner);
                    sb.Append(user.Mention);
                }
            }
            
            var embed = new EmbedBuilder()
                .WithTitle("Game Dev Challenge - Winner")
                .WithDescription(sb.ToString())
                .WithFooter(contest)
                .Build();
            await ReplyAsync("We have a new winner:", embed: embed); 
        }

        [Command("challenge history")]
        [Alias("history")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Shows a the GDFG monthly challenge winners to date.")]
        public async Task HistoryCommand()
        {
            var contestWithWinners = _storage.GetWinnersByContest();

            while (contestWithWinners.Count > 0)
            {
                StringBuilder sb = new StringBuilder("");
                var contests = contestWithWinners.ToArray();

                // Page embeds by max 38 per post.
                for (int i = 0; i < MathF.Min(contests.Length, 38); i++)
                {
                    var contest = contests[i];
                    sb.Append($"{contest.Contest} - ");

                    List<string> winnerNames = new List<string>();
                    foreach (Winner winner in contest.Winners)
                    {
                        IGuildUser user = await Context.Guild.GetUserAsync(winner.UserId);
                        if (user != null)
                        {
                            winnerNames.Add($"**{user.Username}**");
                        }
                        else
                        {
                            // Offline users not stored.
                            // https://stackoverflow.com/questions/64741626/discord-net-getuser-by-id-returns-null
                            winnerNames.Add($"**{winner.Name}**");
                        }
                    }
                    sb.Append(String.Join(", ", winnerNames));
                    sb.AppendLine();

                    contestWithWinners.Remove(contest);
                }

                var embed = new EmbedBuilder()
                    .WithTitle("Game Dev Challenge - Past Winners")
                    .WithDescription(sb.ToString())
                    .WithColor(1f, 0f, 1f)
                    .Build();
                await ReplyAsync("", embed: embed);
            }
        }

        [Command("challenge leaderboard")]
        [Alias("leaders")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Shows a the GDFG monthly challenge winners by their total wins.")]
        public async Task LeaderboardCommand()
        {
            List<WinnerScore> winners = _storage.GetWinnersScores();

            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < winners.Count; i++)
            {
                var winner = winners[i];

                IGuildUser user = await Context.Guild.GetUserAsync(winner.Id);
                if (user != null)
                {
                    sb.Append($"{i+1} - **{user.Username}**: {winner.Score} wins.");
                }
                else
                {
                    sb.Append($"{i+1} - **{winner.Name}**: {winner.Score} wins.");
                }
                sb.AppendLine();
            }

            var embed = new EmbedBuilder()
                .WithTitle("Game Dev Challenge - Leaderboard")
                .WithDescription(sb.ToString())
                .WithColor(0f, 1f, 0f)
                .Build();
            await ReplyAsync("The winners with the most wins are:", embed: embed);

        }

        [Command("challenge streaks")]
        [Alias("streaks")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Shows a the GDFG monthly challenge winners by the length of their streaks.")]
        public async Task StreaksCommand()
        {
            List<WinnerStreak> winners = _storage.GetStreaks();
        
            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < winners.Count; i++)
            {
                var winner = winners[i];
                IGuildUser user = await Context.Guild.GetUserAsync(winner.UserId);
                if (user != null)
                {
                    sb.Append($"{i+1} - **{user.Username}**: {winner.Score} wins.");
                }
                else
                {
                    sb.Append($"{i+1} - **{winner.Name}**: {winner.Score} wins.");
                }

                if (winner.Current)
                {
                    sb.Append(" *current*");
                }

                sb.AppendLine();
            }
        
            var embed = new EmbedBuilder()
                .WithTitle("Game Dev Challenge - Streaks")
                .WithDescription(sb.ToString())
                .WithColor(1f, 0f, 0f)
                .Build();
            await ReplyAsync("These guys are on fire!!", embed: embed);
        }

		[Command("challenge next")]
        [Alias("next")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Next challenge number to be used for winner command.")]
        public async Task NextCommand()
        {
            int challengeCount = _storage.CountChallenges() + _storage.GetOffset();
            StringBuilder sb = new StringBuilder($"The next challenge winner will be: #{challengeCount}");
            await ReplyAsync(sb.ToString());
        }

        [Command("challenge skip")]
        [Alias("skip")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Skip a challenge if there was an episode with no winner.")]
        public async Task SkipCommand()
        {
            int challengeCount = _storage.CountChallenges() + _storage.GetOffset();

            _storage.SkipChallenge();
            
            StringBuilder sb = new StringBuilder($"We are skipping challenge: #{challengeCount}");
            await ReplyAsync(sb.ToString());
        }

        [Command("challenge unskip")]
        [Alias("unskip")]
        [Priority(1000)]
        [RequireGuild]
        [Summary("Rewind a challenge number if skip was used accidentally.")]
        public async Task UnskipCommand()
        {
            _storage.UnskipChallenge();
            int challengeCount = _storage.CountChallenges() + _storage.GetOffset();
            StringBuilder sb = new StringBuilder($"Zack you fool {EmojiExtensions.FromText(":rolling_eyes:")} - we are back to challenge: #{challengeCount}");
            await ReplyAsync(sb.ToString());
        }

        [Command("debug user")]
        [Priority(1000)]
        [RequireElevatedUser]
        public async Task DebugUserAsync(
            params SocketUser[] winUsers)
        {
            if (winUsers.Length < 1)
            {
                await ReplyAsync("Please specify a user to debug.");
                return;
            }
         
            StringBuilder sb = new StringBuilder();

            foreach (var winUser in winUsers)
            {
                sb.Append($"{winUser.Username} -> {winUser.Id}");
                sb.AppendLine();
            }
            
            await ReplyAsync(sb.ToString()); 
        }

    }

}
